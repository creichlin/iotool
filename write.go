package iotool

import (
	"io/ioutil"
	"os"
	"reflect"
)

// WriteChangedFile will write the file only if it does not exist already with the exact same content
// This will prevent changing of the mod date if the content is the same nor trigger any listeners.
//
func WriteChangedFile(filename string, data []byte, perm os.FileMode) error {
	oldData, err := ioutil.ReadFile(filename)
	if err == nil {
		if reflect.DeepEqual(oldData, data) {
			// the content of the file is not changed, leave it as it is
			return nil
		}
	}

	return ioutil.WriteFile(filename, data, perm)
}

package iotool_test

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/creichlin/iotool"
	"gitlab.com/testle/expect"
)

func TestWriteFile(t *testing.T) {
	td, err := ioutil.TempDir("", "goiotooltest")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(td)

	file := filepath.Join(td, "a.txt")
	ctime := func() time.Time {
		st, err := os.Stat(file)
		if err != nil {
			t.Fatal(err)
		}
		return st.ModTime()
	}

	err = iotool.WriteChangedFile(file, []byte("foo"), 0600)
	if err != nil {
		t.Fatal(err)
	}
	old := ctime()

	time.Sleep(time.Millisecond * 10)
	err = iotool.WriteChangedFile(file, []byte("foo"), 0600)
	if err != nil {
		t.Fatal(err)
	}

	expect.Value(t, "old change time", old).ToBe(ctime())

	time.Sleep(time.Millisecond * 10)
	err = iotool.WriteChangedFile(file, []byte("bar"), 0600)
	if err != nil {
		t.Fatal(err)
	}

	expect.Value(t, "old change time", old).NotToBe(ctime())

	nf, _ := ioutil.ReadFile(file)
	expect.Value(t, "new file content", nf).ToBe([]byte("bar"))
}
